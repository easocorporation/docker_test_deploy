yum install docker-io git
service docker start
docker build -t="dockerfile/mongodb" github.com/xetra-enterprize/mongodb
docker run -d --name spider_db dockerfile/mongodb
cd journal_magazine_search/
docker build -t="ieeespider/journal_magazine" .
docker run -ti --link spider_db:spider_db --entrypoint=/journal-magazine/journal_magazine/start.py ieeespider/journal_magazine -s